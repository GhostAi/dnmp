###########################################################
###################### General Setup ######################
###########################################################

### Paths #################################################

# Point to the path of your applications code on your host
APP_CODE_PATH_HOST=../Data/PhpstormProjects

# Point to where the `APP_CODE_PATH_HOST` should be in the container
APP_CODE_PATH_CONTAINER=/var/www

# Choose storage path on your machine. For all storage systems
DATA_PATH_HOST=~/.ghost/data


### Drivers ################################################

# All Networks driver

NETWORKS_DRIVER=bridge

# All volumes driver
VOLUMES_DRIVER=local

### Docker compose files ##################################
COMPOSE_FILE=docker-compose.yml

# Change the separator from : to ; on Windows
COMPOSE_PATH_SEPARATOR=:

# Define the prefix of container names. This is useful if you have multiple projects that use laradock to have seperate containers per project.
COMPOSE_PROJECT_NAME=ghost

TZ=UTC

CHANGE_SOURCE=true
# Select a PHP version of the Workspace and PHP-FPM containers (Does not apply to HHVM).
# Accepted values: 7.4 - 7.3 - 7.2 - 7.1 - 7.0 - 5.6
PHP_VERSION=7.4


### PHP_FPM ###############################################

PHP_INSTALL_BCMATH=true
PHP_INSTALL_MYSQLI=true
PHP_INSTALL_INTL=true
PHP_INSTALL_IMAGEMAGICK=true
PHP_INSTALL_OPCACHE=false
PHP_INSTALL_IMAGE_OPTIMIZERS=true
PHP_INSTALL_PHPREDIS=true
PHP_INSTALL_MEMCACHED=false
PHP_INSTALL_BZ2=true
PHP_INSTALL_GMP=true
PHP_INSTALL_XDEBUG=true
PHP_INSTALL_PCOV=false
PHP_INSTALL_XHPROF=false
PHP_INSTALL_PHPDBG=false
PHP_INSTALL_SMB=false
PHP_INSTALL_IMAP=false
PHP_INSTALL_MONGO=false
PHP_INSTALL_AMQP=false
PHP_INSTALL_CASSANDRA=false
PHP_INSTALL_GEARMAN=false
PHP_INSTALL_MSSQL=false
PHP_INSTALL_SSH2=false
PHP_INSTALL_SOAP=false
PHP_INSTALL_XSL=false
PHP_INSTALL_EXIF=false
PHP_INSTALL_AEROSPIKE=false
PHP_INSTALL_OCI8=false
PHP_INSTALL_PGSQL=false
PHP_INSTALL_GHOSTSCRIPT=false
PHP_INSTALL_LDAP=false
PHP_INSTALL_PHALCON=false
PHP_INSTALL_SWOOLE=true
PHP_INSTALL_TAINT=false
PHP_INSTALL_PG_CLIENT=false
PHP_INSTALL_POSTGIS=false
PHP_INSTALL_PCNTL=true
PHP_INSTALL_CALENDAR=false
PHP_INSTALL_FAKETIME=false
PHP_INSTALL_IONCUBE=false
PHP_INSTALL_RDKAFKA=false
PHP_INSTALL_GETTEXT=false
PHP_INSTALL_APCU=false
PHP_INSTALL_CACHETOOL=false
PHP_INSTALL_YAML=false
PHP_INSTALL_ADDITIONAL_LOCALES=false
PHP_INSTALL_MYSQL_CLIENT=false
PHP_INSTALL_PING=false
PHP_INSTALL_SSHPASS=false
PHP_INSTALL_MAILPARSE=false
PHP_INSTALL_WKHTMLTOPDF=false
PHP_FFMPEG=false
PHP_ADDITIONAL_LOCALES="en_US.UTF-8 es_ES.UTF-8 fr_FR.UTF-8"
PHP_DEFAULT_LOCALE=POSIX

PHP_PUID=1000
PHP_PGID=1000



### MYSQL #################################################

MYSQL_VERSION=latest
MYSQL_DATABASE=default
MYSQL_USER=default
MYSQL_PASSWORD=secret
MYSQL_PORT=3306
MYSQL_ROOT_PASSWORD=root
MYSQL_ENTRYPOINT_INITDB=./mysql/docker-entrypoint-initdb.d


### NGINX #################################################

NGINX_HOST_HTTP_PORT=80
NGINX_HOST_HTTPS_PORT=443
NGINX_HOST_LOG_PATH=./logs/nginx/
NGINX_SITES_PATH=./nginx/sites/
NGINX_PHP_UPSTREAM_CONTAINER=php
NGINX_PHP_UPSTREAM_PORT=9000
NGINX_SSL_PATH=./nginx/ssl/



### REDIS #################################################

REDIS_PORT=6379

### Memcached #############################################

MEMCACHED_VERSION=alpine
MEMCACHED_HOST_PORT=11211
MEMCACHED_CACHE_SIZE=128